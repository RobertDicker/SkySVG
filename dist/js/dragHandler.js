function runEventListeners() {

    let dragSrcEl = null;

    function handleDragStart(e) {
        this.style.opacity = '0.4';

        dragSrcEl = this;

        e.dataTransfer.effectAllowed = 'move';
        e.dataTransfer.setData('text/html', this.innerHTML);
    }

    function handleDragOver(e) {
        if (e.preventDefault) {
            e.preventDefault();
        }

        e.dataTransfer.dropEffect = 'move';

        return false;
    }

    function handleDragEnter(e) {
        this.classList.add('over');
    }

    function handleDragLeave(e) {
        this.classList.remove('over');
    }

    function handleDrop(e) {
        if (e.stopPropagation) {
            e.stopPropagation(); // stops the browser from redirecting.
        }

        if (dragSrcEl != this) {
            dragSrcEl.innerHTML = this.innerHTML;
            this.innerHTML = e.dataTransfer.getData('text/html');
        }

        return false;
    }

    function handleDragEnd(e) {
        this.style.opacity = '1';

        items.forEach(function (item) {
            item.classList.remove('over');
        });
    }


    let items = document.getElementsByClassName('draggable');
    items.forEach(function (item) {
        item.addEventListener('dragstart', handleDragStart, false);
        item.addEventListener('dragenter', handleDragEnter, false);
        item.addEventListener('dragover', handleDragOver, false);
        item.addEventListener('dragleave', handleDragLeave, false);
        item.addEventListener('drop', handleDrop, false);
        item.addEventListener('dragend', handleDragEnd, false);
    });
}




function appendDraggables() {

  let anchor = document.getElementById('continueButton');
  let items = getSelectedArray();

  if (items.length < 1) {
    anchor.setAttribute('href', '#navBarHeader')
    anchor.click();
    return;
  }

  anchor.setAttribute('href', '#orderTokens')


  let holder = document.getElementById("draggableButtons");
  holder.innerHTML = '';

  for (let item of items) {

    let button = document.createElement("button");
    button.classList.add("btn", "btn-lg", "btn-outline-secondary", "m-2", "col-md-2", "draggable");
    button.setAttribute("draggable", "true");
    button.textContent = item;
    holder.append(button);
  }

  runEventListeners();
  anchor.click();

}
