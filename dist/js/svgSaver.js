let img2;


function getSVGBlob(svgEl){
  svgEl.setAttribute("xmlns", "http://www.w3.org/2000/svg");
  const svgData = svgEl.outerHTML;
  const preface = '<?xml version="1.0" standalone="no"?>\r\n';
  return new Blob([preface, svgData], {type: "image/svg+xml;charset=utf-8"});

}


function saveSvgToFile(svgEl, fileName, imageType) {
  if(svgEl.nodeName !== 'svg'){return;}

   console.log("saving " + imageType)
   const Blob = getSVGBlob(svgEl);
  const svgUrl = URL.createObjectURL(Blob);

   //If its an svg just create it with the blob
   if(imageType === 'svg'){

     console.log("Saving svg");
     const downloadLink = document.createElement("a");
     downloadLink.href = svgUrl;
     downloadLink.download = name;
     document.body.appendChild(downloadLink);
     downloadLink.click();
     document.body.removeChild(downloadLink);
     return;
   }
  //Any other image type gets created on a canvas

  saveImage(svgEl, svgUrl, fileName, imageType)

}

function checkAndConvertImagesToBase64(el){

  if(el.nodeName === 'image'){
    getBase64FromUrl(el.getAttribute('href')).then(value => el.setAttribute("href", value));
    return;
  }
  //Not interested in anything other than groups with children
  if(!el.hasChildNodes()){return;}


  for(let childOfEl of el.children){
    checkAndConvertImagesToBase64(childOfEl)
  }


}


//This function creates a temporary canvas, appends the svg, it then saves it as required filetype and removes canvas
function saveImage(svgEl, svgUrl, filename, type) {

  console.log("Trying to save " + filename + "." + type);
  let image = new Image();

  image.onload = () => {

    //create a canvas
    let canvas = document.createElement('canvas');

    canvas.width = svgEl.getBBox().width*100;

    canvas.height = svgEl.getBBox().height*100;
    let context = canvas.getContext('2d');
    // draw image in canvas starting left-0 , top - 0
    context.drawImage(image, 0, 0, canvas.width, canvas.height);
    //  downloadImage(canvas); need to implement

    document.getElementById("image").append(canvas);
    let fileType;
    switch (type) {

      case 'jpeg':
        fileType = canvas.toDataURL('image/jpg');
        break;
      case 'webp':
        fileType = canvas.toDataURL('image/webp');
        break;

      default:
        fileType = canvas.toDataURL();

    }
    download(fileType, filename + "." + type);
    //Remove canvas
    canvas.remove();
  };
  image.src = svgUrl;

}

// This is for downloading the png jpeg webP, a helper function for saveImage
function download(href, name) {
    let link = document.createElement('a');
    link.download = name;
    link.style.opacity = "0";
    document.getElementById("image").append(link);
    link.href = href;
    link.click();
    link.remove();
}
