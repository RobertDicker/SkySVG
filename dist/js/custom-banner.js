let bannerDefaultSettings = {

  height: "89%",
  width: "89%",
  colour: "#414141",
  borderColour: "#2BDC1E",
  fontColour: "#FFFFFF",
  fontFamily: "Arial",
  radius: "0.5%",
  borderWidth: "2%",
  textStrokeWidth: "0.25%",
  textStrokeColour: "none",
  textStyle: "fill:" + "#FFFFFF" + "; font-size: 50%; stroke-width: " + "0.25%" + "stroke: " + "None"
}

let reset = function () {

  let selected = {ticker: "SC", name: "Siacoin"};
  if (tokensMap.size > 1) {
    let checkForSelected = document.querySelector(' #selectContainer input[type=radio]:checked')
    selected = checkForSelected === null ? selected : tokensMap.get(checkForSelected.value);
  }


  let svgContainer = document.getElementById('svgBannerContainer');
  //Remove all children if exists and recreate
  svgContainer.innerHTML = '<svg id="svgBanner" viewBox="0 0 100 100">' +

    '<rect id="rect" rx=' + bannerDefaultSettings.radius + ' x="5%" y="5%" height=' + bannerDefaultSettings.height + '  width=' + bannerDefaultSettings.width + '  fill=' + bannerDefaultSettings.colour +
    ' style= "' + "stroke: " + bannerDefaultSettings.borderColour + "; stroke-width: " + bannerDefaultSettings.borderWidth + '"></rect>' +
    '<g id="interior">' +
    '<text id="line1" style="' + bannerDefaultSettings.textStyle + '" transform="translate(40,20)">' + selected?.name + '</text>' +
    '<text id="line2" style="' + bannerDefaultSettings.textStyle + '" transform="translate(40,30)">' + selected?.ticker + '</text>' +
    '<text id="line3" style="' + bannerDefaultSettings.textStyle + '" transform="translate(40,40)"></text>' +
    '<text id="line4" style="' + bannerDefaultSettings.textStyle + '" transform="translate(40,50)"></text>' +
    '<image href="img/icon/' + selected?.ticker + '.svg" id="tokenImage" height="20%" width="20%" transform="translate(40, 60)"/>' +
    '</g>'
    + '</svg>';

  //Reset all the Colour pickers to defaults
  document.getElementById('borderColourSelector').value = bannerDefaultSettings.borderColour;
  document.getElementById('backgroundColour').value = bannerDefaultSettings.colour;
  document.getElementById('fontColour').value = bannerDefaultSettings.fontColour;
  document.getElementById('demoFont').style.backgroundColor = bannerDefaultSettings.colour;
  document.getElementById('demoFont').style.color = bannerDefaultSettings.fontColour;
  //Reset all checkboxes

  document.getElementById('ratioText').textContent = '';
  document.getElementById('logoCheck').checked = true;
  document.getElementById('nameCheck').checked = true;
  document.getElementById('tickerCheck').checked = true;
  document.getElementById('customCheck').checked = false;

  //Custom input reset
  document.getElementById('customInputDiv').classList.remove("show");
  document.getElementById('customInput').value = "";
  document.getElementById('customInput2').value = "";

  //Reset all sliders
  document.getElementById('borderSlider').value = 20;
  document.getElementById('borderRadiusSlider').value = 10;
  document.getElementById('bannerHeight').value = 100;
  document.getElementById('bannerWidth').value = 100


  document.getElementById('newSVGLocation').innerHTML = '';
}


//Resets the image to how it was once it was created
let restore = function () {

  let background = document.getElementById('rect');
  //Reset Colours
  changeCustomBannerBackgroundColour(bannerData.colour);
  alterBorderColour(bannerData.borderColour);
  setFontColourForCustomBanner(bannerData.fontColour);

  //Reset Border width
  background.style.strokeWidth = bannerData.borderWidth + "%";

  //Reset Sizes of text and logos
  for (let element of document.getElementById('interior').children) {

    if (element.nodeName === "text") {
      element.style.fontSize = "50%";
      element.style.strokeWidth = bannerDefaultSettings.textStrokeWidth;
      element.style.stroke = bannerData.textStrokeColour;

    }
    if (element.nodeName === "image") {
      element.setAttribute('transform', "translate(" + background.getBBox().width / 2 + ", " + background.getBBox().height * (3 / 4) + ")");
    }
  }


  document.getElementById('dragButton').click();
}


function gcd(x, y) {
  if ((typeof x !== 'number') || (typeof y !== 'number'))
    return false;
  x = Math.abs(x);
  y = Math.abs(y);
  while (y) {
    let t = y;
    y = x % y;
    x = t;
  }
  return x;
}

function getRatio(width, height) {

  let gcdRect = gcd(parseInt(width), parseInt(height));

  return [(parseInt(width) / gcdRect), (parseInt(height) / gcdRect)];
}

function resizeSample() {

  let svgBanner = document.getElementById('svgBanner');
  let ratio = getRatio(document.getElementById('bannerWidth').value, document.getElementById('bannerHeight').value);
  let heightPercent = (Math.round(ratio[0] > ratio[1] ? 100 * (ratio[1] / ratio[0]) : 100 * (ratio[1] / ratio[1])));
  let widthPercent = Math.round(ratio[0] > ratio[1] ? 100 * (ratio[0] / ratio[0]) : 100 * (ratio[0] / ratio[1]));

  svgBanner.setAttribute("viewBox", "0 0 " + widthPercent + " " + heightPercent);

  let ratioElement = document.getElementById('ratioText');

  ratioElement.textContent = ratio[0] + " : " + ratio[1];

  //Update the internal rectangle depending on the border size

  let value = document.getElementById('borderSlider').value / 10;
  let rect = document.getElementById('rect');
  console.log(value);
  //If wider
  if (ratio[0] > ratio[1]) {
    rect.setAttribute("height", 99 - 2 * value + "%");
    rect.setAttribute("width", 99 - value + "%");
    rect.setAttribute("x", value / 2 + "%");
    rect.setAttribute("y", value + "%");

  } else {
    rect.setAttribute("height", 99 - value + "%");
    rect.setAttribute("width", 99 - 2 * value + "%");
    rect.setAttribute("x", value + "%");
    rect.setAttribute("y", value / 2 + "%");
  }

  //Move text and logo

  console.log("height: " + svgBanner.viewBox.baseVal.height + "  width" + svgBanner.viewBox.baseVal.width);


  let innerElements = document.getElementById('interior').children;
  console.log("l" + innerElements.length);

  for (let i = 0; i < innerElements.length; i++) {

    innerElements[i].setAttribute('transform', "translate(" + (svgBanner.viewBox.baseVal.width * 0.4) + ", " + (svgBanner.viewBox.baseVal.height * ((i + 2) * 0.1)) + ")");

    console.log(i);

  }
}

function setRadius(radius) {
  document.getElementById('rect').setAttribute("rx", radius + "%");
}

function changeCustomBannerBackgroundColour(colour) {
  document.getElementById('rect').style.fill = colour.toString();
  document.getElementById('demoFont').style.backgroundColor = colour.toString();
  document.getElementById('painterColourLabel').style.borderColor = colour;
  document.getElementById('painterColour').value = colour;
}

function alterBorder(styleName, value) {
  let rect = document.getElementById('rect');

  if (styleName === "stroke-width") {
    rect.style[styleName] = value + "%";
    return
  }

  rect.style[styleName] = value.toString();
}

//This updates both the border and the border value of the element altering it
function alterBorderColour(value) {
  let rect = document.getElementById('rect');
  rect.style.stroke = value.toString();
  document.getElementById('borderColourSelector').value = value;
  document.getElementById('borderColourBtn').style.borderColor = value;
}

let supportedFonts = ["Arial", "Arial Black", "Verdana", "Helvetica", "Tahoma", "Trebuchet MS", "Impact", "Times New Roman", "Didot", "Georgia", "American Typewriter", "Andalé Mono", "Courier", "Lucida Console", "Lucida Sans Unicode", "Monaco", "Bradley Hand", "Brush Script MT", "Luminari", "Comic Sans MS", "Garamond", "Courier New"];
supportedFonts.sort();

function setFontForCustomBanner(selector) {
  for (let element of document.getElementById('interior').children) {

    if (element.nodeName === "text") {
      element.style.fontFamily = selector.options[selector.selectedIndex].value;
    }
  }

  document.getElementById('demoFont').style.fontFamily = selector.options[selector.selectedIndex].value;
}

function setFontColourForCustomBanner(value) {
  for (let element of document.getElementById('interior').children) {

    if (element.nodeName === "text") {

      element.style.fill = value;
    }
  }
  document.getElementById('demoFont').style.color = value;
}


// ======================  THESE ARE ALL THE FUNCTIONS RELATING TO THE LISTENERS FOR CUSTOM BANNERS ==================
function resetListeners(element) {
  let new_element = element.cloneNode(true);
  element.parentNode.replaceChild(new_element, element);
  return new_element;
}


function addListeners(activeFunction) {

  let gChildren = document.getElementById("interior").children;
  for (let i = 0; i < gChildren.length; i++) {
    let newElement = resetListeners(gChildren[i]);

    if (activeFunction === undefined) {
      makeDraggable(newElement);
    } else {
      newElement.addEventListener('click', activeFunction);
    }
    styleCursor(newElement, true);
  }

  let background = resetListeners(document.getElementById('rect'));

  //I only specifically want to paint the background, not adjust its other properties
  if (activeFunction === paint || activeFunction === paintBorder || activeFunction === removeBorder) {
    background.addEventListener('click', activeFunction);
    styleCursor(background, true);
  } else {
    styleCursor(background, false);
  }
}


function styleCursor(element, enabled) {

  if (enabled) {
    element.style.cursor = "pointer";
  } else {
    element.style.cursor = "default";
  }
}

//Listener functions
let paint = function () {

  this.style.fill = document.getElementById('painterColour').value;
}

let removeBorder = function () {

  this.style.strokeWidth = 0;
}

let paintBorder = function () {

  this.style.stroke = document.getElementById('borderColourSelector').value;
}

let resizeUp = function () {
  if (this.nodeName === "text") {
    this.style.fontSize = parseFloat(this.style.fontSize) + 1 + "%";
  } else {
    let dimensionH = this.getAttribute("height");
    let dimensionW = this.getAttribute("width");
    this.setAttribute("height", parseFloat(dimensionH) + 0.5 + "%")
    this.setAttribute("width", parseFloat(dimensionW) + 0.5 + "%")
  }
}

let resizeDown = function () {
  if (this.nodeName === "text") {
    this.style.fontSize = parseFloat(this.style.fontSize) - 1 + "%";
  } else {
    let dimensionH = this.getAttribute("height");
    let dimensionW = this.getAttribute("width");
    this.setAttribute("height", parseFloat(dimensionH) - 0.5 + "%")
    this.setAttribute("width", parseFloat(dimensionW) - 0.5 + "%")
  }
}


//========================= Banner Data Handler ===================================


let bannerData;

//Mainly used to reset back to the users initial settings
function getBannerData() {

  //Size
  let ratio = getRatio(document.getElementById('bannerWidth').value, document.getElementById('bannerHeight').value);
  let heightPercent = (Math.round(ratio[0] > ratio[1] ? 100 * (ratio[1] / ratio[0]) : 100 * (ratio[1] / ratio[1])));
  let widthPercent = Math.round(ratio[0] > ratio[1] ? 100 * (ratio[0] / ratio[0]) : 100 * (ratio[0] / ratio[1]));

  let radius = (document.getElementById('borderRadiusSlider').value).toString();
  let borderWidth = document.getElementById('borderSlider').value / 10;

  //Colours
  let bannerColour = document.getElementById('backgroundColour').value;
  let borderColour = document.getElementById('borderColour').value;
  let fontColour = document.getElementById('fontColour').value;

  //Font
  let fontFamily = "Ariel";

  //Includes
  let tName = document.getElementById('nameCheck').checked;
  console.log(document.getElementById('nameCheck').checked);
  let tTicker = document.getElementById('tickerCheck').checked;
  let tLogo = document.getElementById('logoCheck').checked;
  let customText = "";
  let customTextl2 = "";

  if (document.getElementById('customCheck').checked) {
    customText = document.getElementById('customInput').value;
  }

  if (document.getElementById('customCheck').checked) {
    customTextl2 = document.getElementById('customInput2').value;
  }

  return {
    height: heightPercent,
    width: widthPercent,
    colour: bannerColour,
    borderColour: borderColour,
    fontColour: fontColour,
    fontFamily: fontFamily,
    includeName: tName,
    includeTicker: tTicker,
    includeLogo: tLogo,
    radius: radius,
    borderWidth: borderWidth,
    customText: customText,
    customTextl2: customTextl2,
    textStrokeColour: "none"
  }
}

//Moves the banner and removes uneccessary fields
function prepareBannerForAlignmentPage() {

  let svg = document.getElementById('svgBanner');

  //Move it to the new location
  document.getElementById('newSVGLocation').append(svg);

  //Store banner data for reset;
  bannerData = getBannerData();
  console.log(bannerData);

  // let svg = document.getElementById('bannerSVG');
  let rect = document.getElementById('rect');
  let line1 = document.getElementById('line1');
  let line2 = document.getElementById('line2');
  let line3 = document.getElementById('line3');
  let line4 = document.getElementById('line4');
  let image = document.getElementById('tokenImage');


  if (!bannerData.includeName) {
    line1.remove();
  }

  if (!bannerData.includeTicker) {
    line2.remove();
  }

  console.log(line3.textContent);
  console.log(bannerData.customText.length);
  if (bannerData.customText.length < 1) {
    line3.remove();
  }

  console.log(bannerData.customTextl2.length);
  if (bannerData.customTextl2.length < 1) {
    line4.remove();
  }

  if (!bannerData.includeLogo) {
    image.remove();
  }
}


function prepareCustomBannerForSaving() {

  //Check for images that we can append and conver to base64
  checkAndConvertImagesToBase64(document.getElementById('svgBanner'));

  document.getElementById('image').append(document.getElementById('svgBanner'));
}


function toggleVisibility(element) {


  if(element === undefined){return;}
  if(element === null) {return;}

  if (element.getAttribute("visibility") === 'hidden') {
    element.setAttribute("visibility", "visibile");
  } else {

    element.setAttribute("visibility", "hidden");
  }


}
