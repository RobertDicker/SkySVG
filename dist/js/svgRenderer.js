function drawHeader(height, width, firstLine, secondLine, colours) {

    let headerColour = colours[0].headingColour;
    let headerStroke = "#00000029";
    let titleFontColour = colours[1].titleTextColour;
    let subTitleFontColour = colours[2].subTitleTextColour;

    console.log(headerColour);
    console.log(titleFontColour);
    console.log(subTitleFontColour);

    let headerGroup = document.createElementNS("http://www.w3.org/2000/svg", "g");
    headerGroup.setAttribute("id", "headerGroup");

    let rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
    rect.setAttribute("style", "fill:" + headerColour + ";stroke:" + headerStroke + ";stroke-width:4.70; stroke-miterlimit:4;stroke-dasharray:none");
    rect.setAttribute("x", (width * .22).toString());
    rect.setAttribute("ry", "12");
    rect.setAttribute("y", "4");
    rect.setAttribute("height", (height * 1.2).toString());
    rect.setAttribute("width", (width * 0.73).toString());

    let line2 = document.createElementNS("http://www.w3.org/2000/svg", "text");
    line2.setAttribute("style", "font-style:normal;font-variant:normal;font-weight:normal; font-stretch:normal;font-size:33px;font-family:Impact, serif; fill:" + subTitleFontColour + ";");
    line2.setAttribute("x", (width * 0.40).toString());
    line2.setAttribute("y", (height * 0.90).toString());
    line2.setAttribute("dominant-baseline", "middle");
    line2.setAttribute("text-anchor", "middle");
    line2.textContent = secondLine ? secondLine.toString() : "";

    let line1 = document.createElementNS("http://www.w3.org/2000/svg", "text");
    line1.setAttribute("style", "font-style:normal;font-variant:normal;font-weight:normal; font-stretch:normal;font-size:66px;font-family:Impact, serif; fill:" + titleFontColour + "; stroke:#FFF4; stroke-width:1.5;stroke-miterlimit:4;stroke-dasharray:none");

    line1.setAttribute("y", (height * 0.40).toString());
    line1.textContent = firstLine.toString();
    line1.setAttribute("dominant-baseline", "middle");
    line1.setAttribute("text-anchor", "middle");
    line1.setAttribute("x", (width * 0.58).toString());


    headerGroup.append(rect, line1, line2);
    return headerGroup;

}


function drawRect(token, counter, height, width, colours) {

    let backgroundOfToken = colours;

    let link = document.createElementNS("http://www.w3.org/2000/svg", "a");
    link.setAttribute("href", token?.link ? token.link : "https://error404.org");
    link.setAttribute("style", "text-decoration: none");

    let individualTokenGroup = document.createElementNS("http://www.w3.org/2000/svg", "g");
    individualTokenGroup.setAttribute("id", token.name + "Group");

  let clipPath = document.createElementNS("http://www.w3.org/2000/svg", "clipPath");
  clipPath.setAttribute("id", token.ticker+"Circle");

  let circle = document.createElementNS("http://www.w3.org/2000/svg", "circle");
  circle.setAttribute("cx", ((height/2)+5).toString());
  circle.setAttribute("cy", ((height/2)+10).toString());
  circle.setAttribute("r", ((height/2)-10).toString());
  circle.setAttribute("fill", "#aa3311");
  clipPath.append(circle);

  let defs = document.createElementNS("http://www.w3.org/2000/svg", "defs");
  defs.append(clipPath);
      link.append(defs);


    let rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
    rect.setAttribute("style", "fill:" + backgroundOfToken[counter % backgroundOfToken.length] + ";stroke-width:0.55;stroke-miterlimit:4;stroke-dasharray:none;stroke:#ffffff;stroke-opacity:0.53413653;fill-opacity:1");
    rect.setAttribute("x", "170");
    rect.setAttribute("y", "15");
    rect.setAttribute("height", (height * 0.90).toString());
    rect.setAttribute("ry", "50");
    rect.setAttribute("width", (width * 0.80).toString());


    let image = document.createElementNS("http://www.w3.org/2000/svg", "image");
    image.setAttribute("height", height.toString());
    image.setAttribute("width", height.toString());
    image.setAttribute("x", "5");
    image.setAttribute("y", "10");
    image.setAttribute("clip-path", "url(#"+token.ticker+"Circle)")


  let circleBorder = document.createElementNS("http://www.w3.org/2000/svg", "circle");
  circleBorder.setAttribute("cx", ((height/2)+10).toString());
  circleBorder.setAttribute("cy", ((height/2)+5).toString());
  circleBorder.setAttribute("r", ((height/2)-12).toString());
  circleBorder.setAttribute("fill", "#ffffff")



    let name = document.createElementNS("http://www.w3.org/2000/svg", "text");
    name.setAttribute("style", "font-style:normal;font-variant:normal;font-weight:normal; font-stretch:normal;font-size:70px;font-family:Impact, serif; fill:#ffffff;stroke-width:1.1;stroke-miterlimit:4;stroke-dasharray:none");
    name.setAttribute("x", "320");
    name.setAttribute("y", "150");
    name.setAttribute("id", "text"+token?.name)
    name.setAttribute("dominant-baseline", "middle");
    // name.setAttribute("text-anchor", "middle");
    name.textContent = token.name;


    let symbol = document.createElementNS("http://www.w3.org/2000/svg", "text");
    symbol.setAttribute("style", "font-style:normal;font-variant:normal;font-weight:normal; font-stretch:normal;font-size:130px;font-family:Impact, serif; fill:#ffffff;stroke-width:1.1;stroke-miterlimit:4;stroke-dasharray:none");
    symbol.setAttribute("x", "950");
    symbol.setAttribute("y", "150");
    symbol.setAttribute("dominant-baseline", "middle");
    symbol.setAttribute("text-anchor", "end");
    symbol.textContent = token.ticker;

    //  let imageUrl;
    getBase64FromUrl("img/icon/" + token?.ticker + ".svg").then(value => image.setAttribute("href", value)).then(value => {
        individualTokenGroup.append(rect, circleBorder, image, name, symbol);
        link.append(individualTokenGroup);
      let font = 70
      console.log(token.name + " width before: " + name.getComputedTextLength());
      console.log(token.name + "symbol before: " + symbol.getComputedTextLength());
      while(name.getComputedTextLength() > 630 - symbol.getComputedTextLength() ){
        font = font -1;

        name.setAttribute("style", "font-style:normal;font-variant:normal;font-weight:normal; font-stretch:normal;font-size:" + font +"px;font-family:Impact, serif; fill:#ffffff;stroke-width:1.1;stroke-miterlimit:4;stroke-dasharray:none");
        console.log(token.name + " width adjusted: " + name.getComputedTextLength());

      }

    });




    return link;
}

//I use this in order to save off the contents of the svg
const getBase64FromUrl = async (url) => {
    const data = await fetch(url);
    const blob = await data.blob();
    return new Promise((resolve) => {
        const reader = new FileReader();
        reader.readAsDataURL(blob);
        reader.onloadend = function () {
            const base64data = reader.result;
            resolve(base64data);
        }
    });
}


function renderSVG(selectedTokens, colours, line1, line2) {

    let linesExist = (line1?.length > 0 | line2?.length > 0);

    console.log("rendering");

    //Delete the old one first if it exists
    if (document.getElementById("skyFolio")) {
        document.getElementById("skyFolio").remove();
    }

    let headerHeight = linesExist ? 150 : 0;
    let height = 300;
    let width = 1000;


    let svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");


    svg.setAttribute("viewBox", "0 0 " + width + " " + ((height * selectedTokens.length) + headerHeight));
    svg.setAttribute("preserveAspectRatio", "xMidYMid meet")
    svg.setAttribute("version", "1.1");
    svg.setAttribute("id", "skyFolio");

    //Header

    if (headerHeight !== 0) {
        svg.appendChild(drawHeader(headerHeight, width, line1, line2, colours));
    }


    let tokensGroup = document.createElementNS("http://www.w3.org/2000/svg", "g");
    tokensGroup.setAttribute("id", "tokensGroup");
    svg.appendChild(tokensGroup);

    for (let i = 0; i < selectedTokens.length; i++) {

        if (tokensMap.get(selectedTokens[i]) != null) {
            //Draw each element and add to array

            let tGroup = document.createElementNS("http://www.w3.org/2000/svg", "g");
            tGroup.setAttribute("id", "tGroup");
            tGroup.append(drawRect(tokensMap.get(selectedTokens[i]), i, height, width, colours[3].backgroundColours));
            tGroup.setAttribute("transform", "translate(0 " + (((tokensGroup.childElementCount) * 280) + headerHeight) + ")")
            tokensGroup.appendChild(tGroup);
        }

    }


    document.getElementById("image").append(svg);

    // saveSvg(svg, "test.svg");


}
