let tokensMap = new Map();

function getTokens() {
    console.log("getting tokens");
  let dataPromise = new Promise(function (myResolve) {

    myResolve(getData("tokenData"));
  });

  dataPromise.then(value => {
    console.log("have tokens");
    for (let v of value) {
      tokensMap.set(v.ticker.toString(), v);
    }
    document.getElementById('waitingDiv').remove();
  //  loadTokensToSelection(singleSelection);
  });
}

function loadTokensToSelection(singleSelection) {

  let type = singleSelection ? "radio" : "checkbox";

  for (let token of tokensMap.values()) {

    let input = document.createElement("input");
    input.classList.add("btn-check");
    input.setAttribute("id", token.ticker + "CheckBox");
    input.setAttribute("name", "selectedToken");
    input.setAttribute("autocomplete", "off");
    input.setAttribute("value", token.ticker);
    input.setAttribute("type", type);

    let label = document.createElement("label");
    label.classList.add("btn", "btn-sm", "btn-outline-secondary", "m-1");
    label.setAttribute("for", token.ticker + "CheckBox");

    label.textContent = (token.name);


    document.getElementById("selectContainer").append(input, label);
  }
}



function getColourChoices() {

    const choices = [];
    const backgroundColours = [];

    for (let color of document.querySelectorAll('#backgroundPicker input[type=color]')) {
        backgroundColours.push(color.value);
    }

    let headingColour = document.getElementById('headingColour').value;
    let titleTextColour = document.getElementById('textColourTitle').value;
    let subTitleTextColour = document.getElementById('textColourSubtitle').value;

    choices.push({headingColour: headingColour}, {titleTextColour: titleTextColour}, {subTitleTextColour: subTitleTextColour}, {backgroundColours: backgroundColours});

    return choices
}

function updateExample() {

    let colourBackground = document.getElementById('headingColour').value;
    let titleTextColour = document.getElementById('textColourTitle').value;
    let subTitleTextColour = document.getElementById('textColourSubtitle').value;

    document.getElementById('exampleTitle').setAttribute('style', 'color:' + titleTextColour + '; background: ' + colourBackground);
    document.getElementById('exampleSubTitle').setAttribute('style', 'color:' + subTitleTextColour + '; background: ' + colourBackground);
}


//Data Helpers

function getOrderedElements() {

  let ordered = [];
  let allItems = document.getElementById("draggableButtons");
  allItems.childNodes.forEach(item => {
    ordered.push(item.textContent)
  });
  return ordered;

}

function getSelectedArray() {

  const array = []
  const checkboxes = document.querySelectorAll('#selectContainer input[type=checkbox]:checked')

  for (let i = 0; i < checkboxes.length; i++) {
    array.push(checkboxes[i].value)
  }

  return array;

}

let prepareSelectTokens = function (singleOnly){

  if(tokensMap.size < 1){
    setTimeout(prepareSelectTokens, 1000);
  } else {

    let selectContainer = document.getElementById('selectContainer');
    selectContainer.innerHTML = "";

    if (singleOnly === true) {
      loadTokensToSelection(true);
    } else {
      loadTokensToSelection(false);
    }

    let pageHeading = document.getElementById('selectTokenText');
    let continueButton = document.getElementById('continueButton');
    continueButton.setAttribute("href", "#");


    if (singleOnly) {
      pageHeading.textContent = "Select Your Token";
      continueButton.setAttribute("onclick", "checkATokenHasBeenSelectedForCustomBanner()");
      return;
    }

    pageHeading.textContent = "Select Your Tokens";
    continueButton.setAttribute("onclick", "appendDraggables()");
  }
}

function checkATokenHasBeenSelectedForCustomBanner(){
  let continueButton = document.getElementById('continueButton');
  let selected = document.querySelector(' #selectContainer input[type=radio]:checked').value;
  if (tokensMap.get(selected) === undefined) {
    continueButton.setAttribute('href', '#selectTokens');}
  else {

    reset();
    continueButton.setAttribute('href', '#designOwnBanner')
      };


  continueButton.click();
}


function clearSelected() {
  const checkboxes = document.querySelectorAll('input[type=checkbox]:checked')

  for (let checkbox of checkboxes) {
    checkbox.checked = false;
  }

  document.getElementById('line1').value='';

  document.getElementById('line2').value='';
  document.getElementById('exampleTitle').textContent='';

  document.getElementById('exampleSubTitle').textContent='';

  document.getElementById('additionalColours').innerHTML ='';

  document.getElementById('saveToSkynetButton').disabled = false;

  document.getElementById('directLink').textContent = '';
  document.getElementById('hiddenToggle').click();

  document.getElementById('image').innerHTML = "";

}

function createAnotherBackgroundColour() {

  let div = document.createElement("div");
  div.innerHTML =
    "<div class='row align-self-center col-12'> <input type='color' value='#62656A' class='col mb-4 mr-2 align-self-center'> <span  class='col-8 mr-2'> Every " + ordinal_suffix_of(document.getElementById('additionalColours').childElementCount + 2) + " Color </span><button class='btn-close'  aria-label='Close' onclick='this.parentNode.remove()'></button></div>";

  document.getElementById('additionalColours').append(div);
}


function ordinal_suffix_of(i) {
  let j = i % 10,
    k = i % 100;
  if (j === 1 && k !== 11) {
    return i + "st";
  }
  if (j === 2 && k !== 12) {
    return i + "nd";
  }
  if (j === 3 && k !== 13) {
    return i + "rd";
  }
  return i + "th";
}
